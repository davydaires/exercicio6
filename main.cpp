#include <iostream>

using namespace std;

const int tamanho = 15;

int* vetor[tamanho];
int i;
int j;
int temp;

void inicializarVetor(int* vet[], int tam){

    for(i=0; i < tam; i++){
        cout << "INSIRA UM VALOR PARA A POSICAO " << (i+1) << ": " << endl;
        vet[i] = new int[i];
        cin >> *(vet[i]);
    }
}

void imprimirVetor(int* vet[], int tam){

    for ( i = tam - 1; i >= 0; i--){
        cout << "VALOR INVERTIDO USANDO PONTEIROS: " << *(vet[i]) << endl;
    }
}

int main()
{
inicializarVetor(vetor, tamanho);
imprimirVetor(vetor, tamanho);
return 0;
}
